Version 1.0.0

# AppForm

Recuerda que este proyecto esta en desarrollo, y ya ha sido utilizado en proyectos reales, ya se puede utilizar. sin embargo seguira siendo actualizado.

Este proyecto te ayudara a generar formularios de forma rapido y sencilla, utilizando diseño material
Si quieres saber mas o enterarte de mas cosas seguirme en mis redes sociales, aqui esta mi pagina web.

remiaguilar.com

## Como Instalar

*Requisitos

1. Instalar Angular Material.

ng add @angular/material

Documentacion:
[Angular material](https://material.angular.io/guide/getting-started);

2. Agregar los siguientes modulos de material.
```
import { MatInputModule } from "@angular/material/input";
import { MatDatepickerModule, MatNativeDateModule } from "@angular/material";
import { MatSelectModule } from "@angular/material/select";
import { MatRadioModule } from "@angular/material/radio";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatSliderModule } from "@angular/material/slider";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
```


3. Agregar modulo de formulario.
`import { ReactiveFormsModule } from "@angular/forms";`

4. Agregar Carpeta de forms a tu proyecto.

`proyecto/src/app/form`


5. Importar forms a app.modulo.ts

`import { FormComponent } from "./components/form/form.component";`


## Crear formulario

1. Etiqueta para llamar al formulario:

`<app-form [form_input]="form" [form_config]="formConfig" (output)="formOutput($event)"></app-form>`


## Enviar datos al formulario

1. form_Input: Espera un Array<any>[].
2. form_config: Espera un Objeto{}.
3. output: Regresa un un Objeto.

## Estructura

1. Configuracion del formulario.
```
  formConfig = {
    autocomplete: 'off', //off, on. para activar el autocomplete
    spellcheck: false, //false, true. Para activar la autografia
    error_required: 'Este campo es obligatorio', //Este es el mensaje que saldra si no lo llenan y es obligatorio.
    error_valid: 'Favor de validar este campo', //Este es el mensaje que saldra si el campo no es valido
    class: "d-flex justify-content-center p-2 w-100 flex-wrap" // Aqui se colocan las clases para el form
  }
```

```
  form = [
    {
    type: 'checbox',
      label: '¿Cual es su nombre?',
      value: [], //valor, si se pueden elegir varios valores colocar [], si es un solo dato colocar'';
      required: true, //colcoar este campo si es requerido
      email: true, // colcoar si tiene que ser un correo;
      pattern: '' // colocar el pattern que debe tener el valor
      opcion_multi: true, // Colocar si se puede elegir varias opciones (select)
      select_group: true, // Colocar si se colocaran lista en grupos.
      label_position:'after', //posision del label
      icon_label_after: 'phone',
      icon_label_before: 'phone',
      class_container: "w-100",
      class_label_icon_before: 'mr-2 text-success',
      class_label_icon_after: 'ml-2 text-warning',
      class_label_text: 'text-info',
      class_label:'ml-2',
      class_input: 'p-2',
      text_hint_end: '/200 caracteres maximo',
      text_hint_start: 'Este campo admite cualquier caracter (@,a-z,A-Z)',
      class_text_hint_start: 'text-info mr-2',
      class_text_hint_end: 'text-warning',
      icon_hint_end_before: 'phone',
      class_hint_icon_end_before: 'text-dark',
      icon_hint_end_after: 'phone',
      class_hint_icon_end_after: 'text-info',
      icon_hint_start_before: 'phone',
      class_hint_icon_start_before: 'pl-2 pr-5 text-dark',
      icon_hint_start_after: 'phone',
      class_hint_icon_start_after: 'text-warning',
      hint_length_end: true,
      text_prefix: '$',
      class_text_prefix: 'mr-2',
      icon_prefix_after: 'phone',
      icon_prefix_before: 'phone',
      text_suffix: '.00',
      icon_suffix_before: 'phone',
      icon_suffix_after: 'phone',
      max_length: 500,
      min_length:100,
      //estas son para elegir opciones
      data: [{value:'Opcion *1',disabled:false, position:'after'},{value:'Opcion 2',disabled:true, position:'after'},{value:'Opcion 3',disabled:false, position:'before'}],
      //estas son para elegir opciones en grupos
      data: [
        {
          value: 'Grupo 1',
          disabled: false,
          options: [{value:'Opcion 1',disabled:false},{value:'Opcion 2',disabled:true},{value:'Opcion 3',disabled:false}]
        },
        {
          value: 'Grupo 2',
          disabled: false,
          options: [{value:'Opcion 1',disabled:false},{value:'Opcion 2',disabled:true},{value:'Opcion 3',disabled:false}]
        },
        {
          value: 'Grupo 2',
          disabled: true,
          options: [{value:'Opcion 1',disabled:false},{value:'Opcion 2',disabled:true},{value:'Opcion 3',disabled:false}]
        }
      ]
    }
      }
      ]
```
