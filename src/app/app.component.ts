import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  formConfig = {
    autocomplete: 'off', //off, on. para activar el autocomplete
    spellcheck: false, //false, true. Para activar la autografia
    error_required: 'Este campo es obligatorio', //Este es el mensaje que saldra si no lo llenan y es obligatorio.
    error_valid: 'Favor de validar este campo', //Este es el mensaje que saldra si el campo no es valido
    class: "d-flex justify-content-center p-2 w-100 flex-wrap" // Aqui se colocan las clases para el form
  }


  form = [
    {
      type: 'checbox',
      opcion_multi: true,
      select_group: true,
      label_position:'after',
      label: '¿Cual es su nombre?',
      value: [],
      icon_label_after: 'phone',
      icon_label_before: 'phone',
      class_container: "w-100",
      class_label_icon_before: 'mr-2 text-success',
      class_label_icon_after: 'ml-2 text-warning',
      class_label_text: 'text-info',
      class_label:'ml-2',
      required: true,
      class_input: 'p-2',
      text_hint_end: '/200 caracteres maximo',
      text_hint_start: 'Este campo admite cualquier caracter (@,a-z,A-Z)',
      class_text_hint_start: 'text-info mr-2',
      class_text_hint_end: 'text-warning',
      icon_hint_end_before: 'phone',
      class_hint_icon_end_before: 'text-dark',
      icon_hint_end_after: 'phone',
      class_hint_icon_end_after: 'text-info',
      icon_hint_start_before: 'phone',
      class_hint_icon_start_before: 'pl-2 pr-5 text-dark',
      icon_hint_start_after: 'phone',
      class_hint_icon_start_after: 'text-warning',
      hint_length_end: true,
      text_prefix: '$',
      class_text_prefix: 'mr-2',
      icon_prefix_after: 'phone',
      icon_prefix_before: 'phone',
      text_suffix: '.00',
      icon_suffix_before: 'phone',
      icon_suffix_after: 'phone',
      max_length: 500,
      min_length:100,
      /* data: [{value:'Opcion 1',disabled:false, position:'after'},{value:'Opcion 2',disabled:true, position:'after'},{value:'Opcion 3',disabled:false, position:'before'}], */
/*       data: [
        {
          value: 'Grupo 1',
          disabled: false,
          options: [{value:'Opcion 1',disabled:false},{value:'Opcion 2',disabled:true},{value:'Opcion 3',disabled:false}]
        },
        {
          value: 'Grupo 2',
          disabled: false,
          options: [{value:'Opcion 1',disabled:false},{value:'Opcion 2',disabled:true},{value:'Opcion 3',disabled:false}]
        },
        {
          value: 'Grupo 2',
          disabled: true,
          options: [{value:'Opcion 1',disabled:false},{value:'Opcion 2',disabled:true},{value:'Opcion 3',disabled:false}]
        }
      ] */
    }
  ]


  formOutput(e){
    console.log(e)
  }
  
}