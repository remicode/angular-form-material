import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Validators, FormBuilder, FormControl, FormArray, AbstractControl} from "@angular/forms";
import { map, startWith } from "rxjs/operators";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"]
})
export class FormComponent implements OnInit {  
  
    public data = [];
    public config:any;
  
    //Entradas
    @Input() set form_config(value: Array<any>) {
      this.config = value;
    }
    @Input() set form_input(value: Array<any>) {
      this.data = value;
      this.createForm();
    }
    @Input() set form_reset(value: boolean) {
      if(value){
        value = false;
        this.form.reset();
      }
    }
    @Input() set form_submit(value: boolean) {
      if(value){
        this.form.controls.forEach((i,index) => {
          this.form.controls[index].markAsTouched();
        })
      }
    }
  
    //Salidas
    @Output() output = new EventEmitter();
  
    //Formulario
    public form: FormArray;
  
    constructor(private fb: FormBuilder) {}
  
    ngOnInit() {
    }
  
    //Crear formulario
    createForm() {
      let form = [];
      this.data.forEach((data, index) => {
        const validators = this.getValidators(data, index);
        form.push(new FormControl(data.value ? data.value : '', validators));
      });
      this.form = this.fb.array(form);
      this.createAutocomplete();
    }
  
    //Obtener validaciones
    getValidators(e, index) {
      let data = [];
      if (e.required) {
        data.push(Validators.required);
      }
      if (e.email) {
        data.push(Validators.email);
      }
      if (e.min_length) {
        data.push(Validators.minLength(e.min_length));
      }
      if (e.max_length) {
        data.push(Validators.maxLength(e.max_length));
      }
      if (e.pattern) {
        data.push(Validators.pattern(e.pattern));
      }
      if(e.autocomplete){
        data.push(this.existInArray(e, index));
      }
      return data;
    }
  
    //Checkboxes group
    itemtoArray(event, i, checkbox) {
      const status = event.checked;
      let data: Array<string> = this.form.controls[i].value;
      if (status) {
        data.push(checkbox);
      } else {
        data = data.filter(i => i !== checkbox);
      }
      this.form.controls[i].setValue(data);
    }
  
    //Filtro de autocomplete
    createAutocomplete(){
      this.data.forEach((data, index) => {
        if (data.type === "autocomplete") {
          data.observable = this.form.controls[index].valueChanges.pipe(
            startWith(""),
            map(value => this.filter(value, data.data))
          );
        }
      })
    }
  
    filter(value: string, options): string[] {
      const filterValue = value.toLowerCase();
      return options.filter(option =>
        option.value.toLowerCase().includes(filterValue)
      );
    }
  
    //Validaciones personalizados
    existInArray(data, index) {
      return () => {
        const form = this.form;
        let exist;
        if(form){
          const value = form.controls[index].value;
          exist = data.data.findIndex(i => i === value);
          if(exist < 0) {
            exist = 'No esta correcto';
          }
        }
        return exist;
      };
    }
  
    //Output data
    focusOut(i){
      const data = {
        form: this.form.value,
        valid: this.form.valid
      }
      this.output.emit(data);
    }
  }
  